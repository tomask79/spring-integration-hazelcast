# Scheduling Spring tasks accessing critical section with Hazelcast #

In the company where I work we use [Quartz](http://www.quartz-scheduler.org/) for jobs scheduling. 
Most of the time Quartz is giving us everything we need:

- Scheduling through cron expressions.
- Monitoring and control of launched jobs...

Anyway, sometimes during super high peaks Quartz tables suffers from database locking problem. 
So I was thinking what are the Quartz alternatives. Following projects looks promising:

* Cron4J
* Apache Ignite (oh yes!)

But let's try something light and easy, how about [Spring Scheduling](https://spring.io/guides/gs/scheduling-tasks/) 
for launching the tasks and [Hazelcast](https://hazelcast.com/) datagrid for cluster orchestration? 

## Spring Scheduling and Hazelcast running together ##

Task goal: Let's say I want to launch particular task every 2 seconds on two microservices 
and I want to be sure that there is going to be always *only one task running in the cluster!*

This is the moment when Hazelcast comes handy. We're going to test two Hazelcast solutions for it:

* *ILock* - Hazelcast distributed lock for accessing critical sections. Allows only one thread to be in the critical section.
* *ISemaphore* - Cluster wide counting semaphore for critical section orchestration. Allows more then one thread to be in the critical section. Depends on settings.

Important difference between ILock and ISemaphore distributed objects is that ILock needs to be released by the same thread that asked for lock.
On the other hand ISemaphore can be released by completely another thread. This states where makes sense to use ILock and ISemaphore.

* *ISemaphore* - If you want to unlock critical section asynchronously then ISemaphore is your way.
* *ILock* - Accessing some shared resource where synchronous answer is needed is a way for ILock.

### Demo ###

Let's have two microservices launching every two seconds following job:

```
 private void doJob(final String microServiceName) throws InterruptedException{
        System.out.println(microServiceName + " in critical section...");
        for (int i = 0; i < 4; i++) {
            Thread.sleep(1000);
            System.out.println("Doing something in " + microServiceName);
        }
    }
```

### Demo with ILock ###

And as mentioned, we want to be sure that only thread is in critical section:
(ILock solution)
```
public void performJobWithLock(final String microServiceName) {
        final ILock lock = hazelcastConfiguration.getJobLock();
        if (lock.tryLock()) {
            try {
                doJob(microServiceName);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
	
@Scheduled(fixedRate = 2000)
    public void performJob() {
        jobServices.performJobWithLock("service1");
    }	
```
Notice that lock is obtained before try/finally block (tryLock method). I used tryLock method which is safer in terms of not obtaining the lock, 
because try/finally block is skipped then. But when using ILock.lock() method you need to be aware of possibility that lock wasn't granted, then ILock.unlock()
method raises IllegalStateException. 

### Test ###

* open two terminal windows.
* git clone <this repo url>
* in the top directory with pom.xml run "mvn clean install"
* java -jar spring-microservice-service1/target/service1-0.0.1-SNAPSHOT.war in the first terminal.
* java -jar spring-microservice-service2/target/service2-0.0.1-SNAPSHOT.war in the second terminal.
* after you start both microservices following output will appear in both windows, but *NEVER concurrently* !

First terminal
```
service1 in critical section...
Doing something in service1
Doing something in service1
Doing something in service1
Doing something in service1
```

Second terminal
```
service2 in critical section...
Doing something in service2
Doing something in service2
Doing something in service2
Doing something in service2
```

### Demo with ISemaphore ###

Same as with ILock, but this time ISemaphore is released asynchronously *from another thread*:

```
public void performJobWithSemaphore(final String microServiceName) {
        final ISemaphore semaphore = hazelcastConfiguration.getJobSemaphore();
        try {
            semaphore.acquire();
            Thread thread = new Thread() {
                public void run() {
                    try {
                        try {
                            doJob(microServiceName);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } finally {
                        semaphore.release();
                        System.out.println("Thread released semaphore..."+microServiceName);
                    }
                }
            };
            thread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
```

Don't forget to set number of ISemaphore permits to 1. We want ISemaphore behave as ILock,
but we need to be able to release critical section from another thread.

```
    @Bean
    public ISemaphore getJobSemaphore() {
        ISemaphore semaphore = hazelcastInstance().getSemaphore("criticalJobSemaphore");
        semaphore.init(1);
        return semaphore;
    }
```    

### Test ###

Start both microservices in the same way you did with ILock. Following output will apppear
in the both terminal windows, but again, NEVER concurrently!

First terminal
```
Doing something in service1
Doing something in service1
Doing something in service1
Doing something in service1
Thread released semaphore...service1
```

Second terminal
```
Doing something in service2
Doing something in service2
Doing something in service2
Doing something in service2
Thread released semaphore...service2
```

regards

Tomas













