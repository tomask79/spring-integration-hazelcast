package com.example.service2;

import com.example.hazelcast.HazelcastConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tomask79 on 08.08.17.
 */
@Configuration
public class Config {
    @Bean
    public HazelcastConfiguration getHazelcastConfiguration() {
        final HazelcastConfiguration hazelcastConfiguration = new HazelcastConfiguration();
        return hazelcastConfiguration;
    }
}
