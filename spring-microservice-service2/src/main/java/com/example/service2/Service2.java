package com.example.service2;

import com.example.hazelcast.JobServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by tomask79 on 08.08.17.
 */
@Service
public class Service2 {

    @Autowired
    private JobServices jobServices;

    @Scheduled(fixedRate = 2000)
    public void performJob() {
        //jobServices.performJobWithLock("service2");
        jobServices.performJobWithSemaphore("service2");
    }
}
