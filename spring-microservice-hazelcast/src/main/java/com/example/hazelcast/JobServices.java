package com.example.hazelcast;

import com.hazelcast.core.ILock;
import com.hazelcast.core.ISemaphore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tomask79 on 10.08.17.
 */
@Configuration
public class JobServices {

    @Autowired
    private HazelcastConfiguration hazelcastConfiguration;

    public void performJobWithLock(final String microServiceName) {
        final ILock lock = hazelcastConfiguration.getJobLock();
        if (lock.tryLock()) {
            try {
                doJob(microServiceName);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public void performJobWithSemaphore(final String microServiceName) {
        final ISemaphore semaphore = hazelcastConfiguration.getJobSemaphore();
        try {
            semaphore.acquire();
            Thread thread = new Thread() {
                public void run() {
                    try {
                        try {
                            doJob(microServiceName);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } finally {
                        semaphore.release();
                        System.out.println("Thread released semaphore..."+microServiceName);
                    }
                }
            };
            thread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doJob(final String microServiceName) throws InterruptedException{
        System.out.println(microServiceName + " in critical section...");
        for (int i = 0; i < 4; i++) {
            Thread.sleep(1000);
            System.out.println("Doing something in " + microServiceName);
        }
    }
}
