package com.example.hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.stereotype.Service;

/**
 * Created by tomask79 on 04.08.17.
 */
@Service
public class HazelcastConfiguration {

    @Bean
    public HazelcastInstance hazelcastInstance() {
        final Config config = new Config();
        return Hazelcast.newHazelcastInstance(config);
    }

    @Bean
    public ILock getJobLock() {
        ILock lock = hazelcastInstance().getLock("criticalJob");
        return lock;
    }

    @Bean
    public ISemaphore getJobSemaphore() {
        ISemaphore semaphore = hazelcastInstance().getSemaphore("criticalJobSemaphore");
        semaphore.init(1);
        return semaphore;
    }
}
