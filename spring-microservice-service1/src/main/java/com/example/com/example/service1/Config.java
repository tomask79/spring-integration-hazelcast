package com.example.com.example.service1;

import com.example.hazelcast.HazelcastConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by tomask79 on 07.08.17.
 */
@Configuration
public class Config {

    @Bean
    public HazelcastConfiguration getHazelcastConfiguration() {
        final HazelcastConfiguration hazelcastConfiguration = new HazelcastConfiguration();
        return hazelcastConfiguration;
    }
}
