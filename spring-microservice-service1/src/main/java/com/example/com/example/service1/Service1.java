package com.example.com.example.service1;

import com.example.hazelcast.HazelcastConfiguration;
import com.example.hazelcast.JobServices;
import com.hazelcast.core.ILock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by tomask79 on 08.08.17.
 */
@Service
public class Service1 {

    @Autowired
    private JobServices jobServices;

    @Scheduled(fixedRate = 2000)
    public void performJob() {
        //jobServices.performJobWithLock("service1");
        jobServices.performJobWithSemaphore("service1");
    }
}
